# ManoMotion-GameJam
A game made in ManoMotion &amp; OGL collab game jam.

Our contributions/project can be found in the project at:

ManoMotion-GameJam/Assets/Resources/Spooky/

The correct scene to load/build for the game is called "Gestures".
(Don't mind about the file naming and confusing layout, it happens in a hurry.)