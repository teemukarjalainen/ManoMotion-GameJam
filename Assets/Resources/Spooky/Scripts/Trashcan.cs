﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trashcan : MonoBehaviour {

    public GameObject ballSpawnerObject;
    BallSpawner ballSpawner;
    public GameObject pointControllerObject;
    PointController pointController;

    // Use this for initialization
    void Start () {
        pointController = pointControllerObject.GetComponent<PointController>();
        ballSpawner = ballSpawnerObject.GetComponent<BallSpawner>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider collision)
    {
        GameObject gameObject = collision.gameObject;
        Ball ball = gameObject.GetComponent<Ball>();

        if (ball.released)
        {
            gameObject.GetComponent<Collider>().enabled = false;

            if (ball.isBomb)
            {
                Debug.Log("MyGameTag: Yay, points for you!");
                pointController.addPoints();
            }
            else
            {
                Debug.Log("MyGameTag: FAIL! Don't delete your decorations!");
                AudioManager.instance.PlaySingle(AudioManager.instance.ornamentBreak);
                pointController.removePoints();
            }

            Destroy(gameObject);
            ballSpawner.ballSpawned = false;
        }
    }
}
