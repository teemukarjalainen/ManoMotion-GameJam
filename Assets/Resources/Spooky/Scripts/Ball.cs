﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public ManoGestureTrigger mytrigger;

    private static ILogger logger = Debug.unityLogger;
    private static string kTAG = "MyGameTag";

    public bool isBomb;
    public bool clickable = true;
    public bool released = true;

    public Vector3 screenPoint;
    private Vector3 offset;

    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;
    //public Model mesh;

    public Material mat1, yellow, blue;
    void Start()
    {
        meshRenderer = this.GetComponent<MeshRenderer>();

        int random = Random.Range(0, 100);
        Debug.Log(random);
        
        if (random >= 80)
        {
            isBomb = true;
            meshRenderer.material = mat1;
        }
        else if (random <= 80 && random >= 60)
        {
            meshRenderer.material = yellow;
            isBomb = false;
        }
        else if (random <= 60 && random >= 30)
        {
            meshRenderer.material = blue;
            isBomb = false;
        }
        else if (random <= 30 && random >= 0)
        {
            isBomb = false;
        }

    }

    void Update()
    {
        GestureInfo gesture = ManomotionManager.Instance.Hand_infos[0].hand_info.gesture_info;
        ManoGestureTrigger currentlyDetectedTrigger = gesture.mano_gesture_trigger;
        TrackingInfo tracking = ManomotionManager.Instance.Hand_infos[0].hand_info.tracking_info;
        //TriggerThings(currentlyDetectedTrigger, tracking);
        ContinuousGesture(gesture, tracking);

    }


    void ContinuousGesture(GestureInfo gesture, TrackingInfo tracking)
    {
        if (clickable)
        {
            if (gesture.mano_gesture_continuous == ManoGestureContinuous.HOLD_GESTURE)
            {
                //Handheld.Vibrate();
                Vector3 boundingBoxCenter = tracking.bounding_box_center;
                boundingBoxCenter.z = 9.5f;
                Debug.Log("MyGameTag: pos " + Camera.main.ViewportToWorldPoint(boundingBoxCenter));
                gameObject.transform.position = Camera.main.ViewportToWorldPoint(boundingBoxCenter);
                released = false;

            }
            else if (gesture.mano_gesture_continuous == ManoGestureContinuous.OPEN_PINCH_GESTURE)
            {
                released = true;
            }
        }

    }

    /*void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(transform.position);
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        released = false;
    }*/

    /*private void OnMouseUp()
    {
        released = true;
    }*/

    /*void OnMouseDrag()
    {
        if (clickable)
        {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            transform.position = curPosition;
        }
    }*/
}
