﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//AudioManager.instance.PlaySingle(AudioManager.instance.wolfHowl);

public class AudioManager : MonoBehaviour {
    public static AudioManager instance = null;     //Allows other scripts to call functions from SoundManager.             

    public AudioSource efxSource, musicSource, interfaceSource;

    public AudioClip music;

    public AudioClip ornamentBreak;
    public AudioClip bombExplosion;
    public AudioClip victory;

    public float lowPitchRange = .95f;              //The lowest a sound effect will be randomly pitched.
    public float highPitchRange = 1.05f;            //The highest a sound effect will be randomly pitched.
    
    public AudioClip[] stepBool;
    
    public float soundRate = 5;
    public float nextSound;

    public float stepSoundRate = 0.1f;
    private float nextStepSound;
    private float audio2Volume = 0.0f;

    // Use this for initialization
    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
        {
            //if not, set it to this.
            instance = this;
        }
        //If instance already exists:
        else if (instance != this)
        {
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);
        }

        /*
        ambientPool = Resources.LoadAll<AudioClip>("Audio/Animals/Ambient/");
        stepBool = Resources.LoadAll<AudioClip>("Audio/HumanCreated/Walk/");

        worldMusic = Resources.Load<AudioClip>("Audio/Music/Nordic landscape");
        */
        nextSound = Time.time + soundRate;

    }


    // Update is called once per frame
    void Update () {
        fadeIn(musicSource);
        
    }

    //Used to play single sound clips from other sources than player.
    public void PlaySingle(AudioClip clip)
    {
        if (clip != null)
        {
            if (interfaceSource.loop == true)
            {
                interfaceSource.loop = false;
            }
            //Set the clip of our efxSource audio source to the clip passed in as a parameter.
            interfaceSource.clip = clip;

            //Play the clip.
            interfaceSource.Play();
        }
    }

    public void PlaySingleUi(AudioClip clip)
    {
        if (interfaceSource.loop == true)
        {
            interfaceSource.loop = false;
        }
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        interfaceSource.clip = clip;

        //Play the clip.
        interfaceSource.Play();
    }

    //Used to play single sound clips.
    public void PlayLoop(AudioClip clip)
    {
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        efxSource.clip = clip;

        //Play the clip.
        efxSource.loop = true;
        efxSource.Play();
    }

    public void PlayLoopMusic()
    {
        audio2Volume = 0.0f;
        musicSource.volume = 0.0f;
        //Set the clip of our musicSource audio source to the clip passed in as a parameter.
        musicSource.clip = music;

        //Play the clip.
        musicSource.loop = true;
        musicSource.Play();
    }

    //RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
    public void RandomizeSfx(params AudioClip[] clips)
    {
        //Generate a random number between 0 and the length of our array of clips passed in.
        int randomIndex = Random.Range(0, clips.Length);

        //Choose a random pitch to play back our clip at between our high and low pitch ranges.
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        //Set the pitch of the audio source to the randomly chosen pitch.
        efxSource.pitch = randomPitch;

        //Set the clip to the clip at our randomly chosen index.
        efxSource.clip = clips[randomIndex];

        //Play the clip.
        efxSource.Play();
    }

    public void fadeIn(AudioSource audioSource)
    {
        if (audioSource.volume < 1)
        {
            audio2Volume += 0.3f * Time.deltaTime;
            audioSource.volume = audio2Volume;
        }
    }
}
