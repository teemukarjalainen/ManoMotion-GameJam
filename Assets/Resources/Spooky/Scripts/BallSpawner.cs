﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour {

    public bool ballSpawned = false;

    public GameObject ballObject;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if (!ballSpawned)
        {
            spawnBall();
        }
	}

    void spawnBall()
    {
        Instantiate(ballObject, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 2f, gameObject.transform.position.z), ballObject.transform.rotation);

        ballSpawned = true;
    }
}
