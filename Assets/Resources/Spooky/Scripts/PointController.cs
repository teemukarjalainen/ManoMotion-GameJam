﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class PointController : MonoBehaviour {
    public ManoGestureTrigger mytrigger;

    public TextMeshProUGUI pointsText;
    public TextMeshProUGUI timeText;

    public GameObject gameOverPanel;
    public TextMeshProUGUI gameOverPointsText;

    public float targetTime = 60.0f;
    public int points = 0;

    bool timerStopped = false;
    bool uiMusicPlayed = false;

    // Use this for initialization
    void Start () {
        pointsText.text = "0";
        timeText.text = "0";
    }

    void Update()
    {
        if(!AudioManager.instance.musicSource.isPlaying)
        {
            AudioManager.instance.PlayLoopMusic();
        }

        if (!timerStopped)
        {
            targetTime -= Time.deltaTime;
            timeText.text = targetTime.ToString("F1");
        }

        if (targetTime <= 0.0f)
        {
            timerEnded();
        }
        GestureInfo gesture = ManomotionManager.Instance.Hand_infos[0].hand_info.gesture_info;
        ManoGestureTrigger currentlyDetectedTrigger = gesture.mano_gesture_trigger;
        // TriggerThings(currentlyDetectedTrigger);
    }

    public void addPoints()
    {
        points++;
        pointsText.text = points.ToString();
    }
    
    public void removePoints()
    {
        points--;
        pointsText.text = points.ToString();
    }

    public void clearPoints()
    {
        points = 0;
        pointsText.text = points.ToString();
    }

    public void restartGame()
    {
        SceneManager.LoadScene("Gestures");
    }

    void timerEnded()
    {
        if (!AudioManager.instance.interfaceSource.isPlaying && !uiMusicPlayed)
        {
            AudioManager.instance.PlaySingleUi(AudioManager.instance.victory);
            uiMusicPlayed = true;
        }

        AudioManager.instance.musicSource.Stop();

        //do your stuff here.
        timerStopped = true;
        gameOverPanel.SetActive(true);
        gameOverPointsText.text = "Score: " + points.ToString();

        GameObject[] balls;
        balls = GameObject.FindGameObjectsWithTag("Ball");

        foreach (GameObject ball in balls)
        {
            ball.GetComponent<Ball>().clickable = false;
        }
    }

    void TriggerThings(ManoGestureTrigger someTrigger)
    {
        if (someTrigger == mytrigger)
        {
            restartGame();
        }

    }
}
