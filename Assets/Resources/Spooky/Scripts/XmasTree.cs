﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XmasTree : MonoBehaviour {

    public GameObject ballSpawnerObject;
    BallSpawner ballSpawner;
    public GameObject pointControllerObject;
    PointController pointController;

    // Use this for initialization
    void Start()
    {
        pointController = pointControllerObject.GetComponent<PointController>();
        ballSpawner = ballSpawnerObject.GetComponent<BallSpawner>();
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnTriggerStay(Collider collision)
    {
        GameObject gameObject = collision.gameObject;
        Ball ball = gameObject.GetComponent<Ball>();

        if (ball.released)
        {
            ball.clickable = false;
            gameObject.GetComponent<Collider>().enabled = false;
            ballSpawner.ballSpawned = false;

            if (ball.isBomb)
            {
                Debug.Log("MyGameTag: BoOM");
                AudioManager.instance.PlaySingle(AudioManager.instance.bombExplosion);
                destroyAllBalls();
                pointController.clearPoints();
            }
            else
            {
                Debug.Log("MyGameTag: Yay, points for you!");
                pointController.addPoints();
            }
        }
    }

    public void destroyAllBalls()
    {
        GameObject[] balls;
        balls = GameObject.FindGameObjectsWithTag("Ball");

        foreach (GameObject ball in balls)
        {
            ball.GetComponent<Rigidbody>().useGravity = true;

            Destroy(ball, 2.0f);
        }
    }


}
